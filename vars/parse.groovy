#!/usr/bin/groovy
def call(body){
    // evaluate the body block, and collect configuration into the object
    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

    try{
        //Parse a json file.
        File f = new File(config.jsonFile)
        def slurper = new JsonSlurper()
        def jsonText = f.getText()
        json = slurper.parseText( jsonText )
        echo "The json file we are parsing is ${config.json}"
        echo "The output is $json"
    }catch(e){
        echo 'There was an error!'
    }
}