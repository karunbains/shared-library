#!/usr/bin/groovy
def call(body) {
    // evaluate the body block, and collect configuration into the object
    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

    try{
        echo 'This is is shared library'
        echo '--WELCOME---'
        echo "print config ${config.username}"
    }catch(e){
        echo 'There was an error'
        currentBuild.result = "FAILED"
        throw e
    }
}